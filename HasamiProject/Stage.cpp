#include "Stage.h"
#include "Engine/Model.h"

//コンストラクタ
Stage::Stage(GameObject* parent)
    :GameObject(parent, "Stage")
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
    hModel_ = Model::Load("Stage.fbx");

    //左右反転
    transform_.rotate_.y = 180;

    //コライダー
    PolygonCollider pc1;
    pc1.AddPoint(-5, 0);
    pc1.AddPoint(7, 0);
    pc1.AddPoint(7, -1);
    pc1.AddPoint(-5, -1);
    colliders_.push_back(pc1);

    PolygonCollider pc2;
    pc2.AddPoint(4, 0);
    pc2.AddPoint(7, 2);
    pc2.AddPoint(7.5f, 1);
    pc2.AddPoint(3.5f, -1.5);
    colliders_.push_back(pc2);

    PolygonCollider pc3;
    pc3.AddPoint(7, 4);
    pc3.AddPoint(10, 4);
    pc3.AddPoint(10, 1);
    pc3.AddPoint(7, 1);
    colliders_.push_back(pc3);

}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
    Model::SetTransform(hModel_, transform_);
    Model::Draw(hModel_);
}

//開放
void Stage::Release()
{
}

//指定した位置がステージに当たってるかチェック
bool Stage::IsHit(XMFLOAT3 position)
{
    //全てのコライダーとチェックする
    for (int i = 0; i < colliders_.size(); i++)
    {
        if (colliders_[i].Hit(position))
        {
            //当たってる
            return true;
        }
    }

    //当たってない
    return false;
}

bool Stage::IsHit(PolygonCollider target, XMMATRIX world)
{
    //全てのコライダーとチェックする
    for (int i = 0; i < colliders_.size(); i++)
    {
        if (colliders_[i].Hit(&target, world))
        {
            //当たってる
            return true;
        }
    }

    //当たってない
    return false;
}

void Stage::GetNormal(XMFLOAT3 p1, XMFLOAT3 p2, XMFLOAT3* normal, XMFLOAT3* hitPoint)
{
    //全てのコライダーとチェックする
    for (int i = 0; i < colliders_.size(); i++)
    {
        if (colliders_[i].GetNormal(p1, p2, normal, hitPoint))
        {
            break;
        }
    }
}
