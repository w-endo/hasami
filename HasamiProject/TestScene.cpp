#include "TestScene.h"
#include "Engine/Camera.h"
#include "Scissors.h"
#include "Stage.h"


//コンストラクタ
TestScene::TestScene(GameObject * parent)
	: GameObject(parent, "TestScene")
{
}

//初期化
void TestScene::Initialize()
{
	//ステージ
	Instantiate<Stage>(this);

	//ハサミ本体
	Instantiate<Scissors>(this);


	Camera::SetPosition(XMFLOAT3(5, 5, -10));
	Camera::SetTarget(XMFLOAT3(5, 3, 0));
}

//更新
void TestScene::Update()
{
}

//描画
void TestScene::Draw()
{
}

//開放
void TestScene::Release()
{
}
