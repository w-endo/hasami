#include "Scissors.h"
#include "Engine/Input.h"


//コンストラクタ
Scissors::Scissors(GameObject* parent)
    :GameObject(parent, "Scissors"), move_(XMFLOAT3(0,0,0))
{
}

//デストラクタ
Scissors::~Scissors()
{
}

//初期化
void Scissors::Initialize()
{
    pBlade_L = Instantiate<Blade>(this);    //左刃
    pBlade_L->Load(0);


    pBlade_R = Instantiate<Blade>(this);    //右刃
    pBlade_R->Load(1);

    //テスト用
    //pBlade_R->isPrick = true;   //右が地面に刺さってるとして
    pBlade_L->SetRotateZ(-90);

    //初期位置
    transform_.position_ = XMFLOAT3(1.0f, 3.0f, 0);

}

//更新
void Scissors::Update()
{
    //ハサミの開閉
    OpenClose();

    //ハサミの回転
    Rotation();

    //左右移動
    Move();

    //ジャンプと落下
    JumpAndFall();
}




//ハサミの開閉
void Scissors::OpenClose()
{
    int angle = 0;

    if (Input::IsKey(DIK_W))    //開く
    {
        angle = 1;
    }
    if (Input::IsKey(DIK_S))    //閉じる
    {
        angle = -1;
    }

    //開閉
    pBlade_L->Open(-angle);
    pBlade_R->Open(angle);


    //角度制限
    XMVECTOR l = { 1,0,0,0 };
    XMVECTOR r = { 1,0,0,0 };
    XMMATRIX rotateL = XMMatrixRotationZ(XMConvertToRadians(pBlade_L->GetRotate().z));
    XMMATRIX rotateR = XMMatrixRotationZ(XMConvertToRadians(pBlade_R->GetRotate().z));
    l = XMVector3TransformCoord(l, rotateL);
    r = XMVector3TransformCoord(r, rotateR);
    float a = acos(XMVectorGetX(XMVector3Dot(l, r)));

    if (XMConvertToDegrees(a) > 170 || XMConvertToDegrees(a) < 2)    //170度以上または2度未満ならもどす
    {
        pBlade_L->Open(angle);
        pBlade_R->Open(-angle);
    }
}

//ハサミの回転
void Scissors::Rotation()
{
    Blade* pPrickBlade = nullptr;   //刺さってる方の刃

    if (pBlade_L->IsPrick() && pBlade_R->IsPrick())     //両方刺さってたら回転できない
    {
        return;
    }
    else if (pBlade_L->IsPrick())       //左が刺さってる
    {
        pPrickBlade = pBlade_L;
    }
    else        //右が刺さってる
    {
        pPrickBlade = pBlade_R;
    }

    //刺さってる刃の先端位置（回転前）
    XMFLOAT3 prevPivotPoint = pPrickBlade->GetTipPoint();


    //回転
    int angle = 0;
    if (Input::IsKey(DIK_LEFT))
    {
        angle = 1;
    }
    if (Input::IsKey(DIK_RIGHT))
    {
        angle = -1;
    }
    transform_.rotate_.z += angle;



    //刺さってる刃の先端位置（回転後）
    XMFLOAT3 nowPivotPoint = pPrickBlade->GetTipPoint();

    //先端位置がずれた分移動させて戻す
    transform_.position_.x += prevPivotPoint.x - nowPivotPoint.x;
    transform_.position_.y += prevPivotPoint.y - nowPivotPoint.y;
    transform_.position_.x += prevPivotPoint.z - nowPivotPoint.z;
}

//左右移動
void Scissors::Move()
{
    if (!pBlade_L->IsPrick() && !pBlade_R->IsPrick())     //どっちも刺さってなければ
    {
        if (Input::IsKey(DIK_D))
        {
            //transform_.position_.x += 0.1f;
            move_.x += 0.02f;
        }
        if (Input::IsKey(DIK_A))
        {
            //transform_.position_.x -= 0.1f;
            move_.x -= 0.02f;
        }
    }
}

//ジャンプと落下
void Scissors::JumpAndFall()
{
    //どっちも刺さってなければ
    if (!pBlade_L->IsPrick() && !pBlade_R->IsPrick())
    {
        //落下
        transform_.position_.x += move_.x;
        transform_.position_.y += move_.y;
        transform_.position_.z += move_.z;

        //重力
        move_.y -= 0.05f;
    }

    //どっちか刺さってら
    else
    {
        //動きを止める
        move_.x = 0;
        move_.y = 0;
        move_.z = 0;

        //ジャンプ
        if (Input::IsKeyDown(DIK_SPACE))
        {
            //地面の法線よりちょっと上向きにジャンプ
            move_.x = jumpDirection_.x * 0.3f;
            move_.y = jumpDirection_.y * 0.5f;
            move_.z = jumpDirection_.z * 0.3f;

            //ここで１回動かしておかないと、次のフレームでも刺さったままで飛べない
            transform_.position_.x += move_.x;
            transform_.position_.y += move_.y;
            transform_.position_.z += move_.z;

            //いったん抜く
            pBlade_L->isPrick = false;
            pBlade_R->isPrick = false;
        }
    }
}

//描画
void Scissors::Draw()
{
}

//開放
void Scissors::Release()
{
}

void Scissors::Reflection()
{
    transform_.position_.x -= move_.x;
    transform_.position_.y -= move_.y;

    move_.x *= -0.5f;
    move_.y *= -0.5f;
}
